#!/usr/bin/env python

import socket
import struct
import json
from socket import error as SocketError
import Queue
import threading
import os

TCP_IP = '0.0.0.0'
TCP_PORT = 5006
BUFFER_SIZE = 20  # Normally 1024, but we want fast response

class TCPServerWait:
	def __init__(self):
		self.s = socket.socket(socket.AF_UNIX,socket.SOCK_STREAM)
		try:
			os.remove('/tmp/robogui')
		except OSError: pass
		self.s.bind('/tmp/robogui')
		self.connected = False
		self.waitForConnection()
	def waitForConnection(self):
		self.s.listen(1)
		self.conn, self.addr = self.s.accept()
		print 'Connection address:', self.addr
		self.connected = True
	def sendMessage(self,message):
		if not self.connected: return
		try:
			self.conn.send(message+"\n")
			self.waitForResponse()
		except SocketError as e:
			print 'Connection error'
			self.connected = False
			self.conn.close()
			self.waitForConnection()
	def waitForResponse(self):
		try:
			data = self.conn.recv(20)
			if not data:
				print 'Connection lost'
				self.connected = False
				self.conn.close()
				self.waitForConnection()
		except SocketError as e:
			#This catches the case where the client crashes for some reason
			print 'Connection error'
			self.connected = False
			self.conn.close()
			self.waitForConnection()

def worker():
	svr = TCPServerWait()
	while True:
		item = q.get()
		svr.sendMessage(item)
		q.task_done()

def send(message):
	try:
		q.put(message,False)	
	except:
		pass

	


q = Queue.Queue(maxsize=1)
t = threading.Thread(target=worker)
t.daemon = True
t.start()



#!/bin/bash

NODE=/home/pi/server/node/bin/node
SERVER_JS=/home/pi/server/brickpiexplorer/app.js
SERVER_JS_DIR=/home/pi/server/brickpiexplorer/
SERVER_JS_FILE=app.js
USER=pi
OUT=/home/pi/nodejs.log

case "$1" in

start)
	#echo "pulling new version of server"
	#cd $SERVER_JS_DIR
	#sudo -u $USER git pull
	echo "starting node: $NODE $SERVER_JS_FILE"
	sudo -u $USER $NODE $SERVER_JS > $OUT 2>$OUT &
	;;

stop)
	killall $NODE
	;;

*)
	echo "usage: $0 (start|stop)"
esac

exit 0
